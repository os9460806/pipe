#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main()
{
    int pipe1[2], pipe2[2];
    pid_t pid;
    if (pipe(pipe1) == -1 || pipe(pipe2) == -1) {
        perror("Pipe error");
        return 1;
    }

    pid = fork();
    if (pid < 0) {
        perror("Fork error");
        return 1;
    }
    if (pid == 0){
        close(pipe1[1]);
        char input_str[100];
        read(pipe1[0], input_str,100);
        int i;
        for(i = 0; i < strlen(input_str); i++){
            input_str[i] = toupper(input_str[i]);
        }
        close(pipe2[0]);
        write(pipe2[1], input_str, strlen(input_str));
    }else{
        close(pipe1[0]);
        char str[] = "Hello";
        write(pipe1[1], str, strlen(str));
        close(pipe2[1]);
        
        char output_str[100];
        read(pipe2[0],output_str,100);
        wait(NULL);
        printf("Child: %s\nParent: %s\n",str,output_str);
    }
    return 0;
}